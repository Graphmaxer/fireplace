﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditScroll : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 10f;
    Vector3 target;
    void Start()
    {
        target = new Vector3(0,2000,0);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene("MainMenu");
        }
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
    }
}
