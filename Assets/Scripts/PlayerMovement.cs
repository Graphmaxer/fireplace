﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int speed = 5;
    void Start()
    {
    }

    void Update()
    {
        var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        transform.position += move * speed * Time.deltaTime;    
    }

    public int GetSpeed()
    {
        return speed;
    }
}
