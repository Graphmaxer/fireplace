﻿using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] 
    public float speed = 2f;

    [SerializeField] 
    public Transform[] waypoint;
    
    private int Index = 0;

    void Start()
    {
        transform.position = Vector2.MoveTowards(transform.position, 
            waypoint[Index].transform.position,
            Time.deltaTime * speed);
    }

    void Update()
    {
        Move();
    }
    void Move()
    {
        if ((Vector2)transform.position == (Vector2)waypoint[Index].transform.position) {
            Index += 1;
        }
        if (Index == waypoint.Length)
            Index = 0;
        transform.position = Vector2.MoveTowards (transform.position,
            waypoint[Index].transform.position,
            speed * Time.deltaTime);
    }

    public void SetIndex(int newIndex)
    {
        Index = newIndex;
    }

    public void AddFirstWaypoint(Transform itemToAdd)
    {
        Transform[] finalArray = new Transform[1];
        finalArray[0] = itemToAdd;
        waypoint = finalArray;
    }
}
