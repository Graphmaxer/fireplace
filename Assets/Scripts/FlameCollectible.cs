﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameCollectible : MonoBehaviour
{
    public float distance = 1f;

    private Transform player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (Vector3.Distance(transform.position, player.position) <= distance)
        {
            Destroy(gameObject);
        }
    }
}
