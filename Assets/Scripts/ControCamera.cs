﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControCamera : MonoBehaviour
{
    public GameObject player;       //Public variable to store a reference to the player game object
    public float right;
    private float left ;
    private float up ;
    public float down ;
    private Vector3 intermed;

    private Vector3 offset;         //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = transform.position - player.transform.position;
        GameObject limite1 = GameObject.FindGameObjectWithTag("limite1");
        left = limite1.transform.position.x;
        up = limite1.transform.position.y;
        GameObject limite2 = GameObject.FindGameObjectWithTag("limite2");
        right = limite2.transform.position.x;
        down = limite2.transform.position.y;

    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        //transform.position = player.transform.position + offset;
        intermed = player.transform.position + offset;
        if (intermed.x > right)
        {
            Vector3 temp = new Vector3(right, intermed.y , -10f);
            intermed = temp;
        }
        if (intermed.x < left)
        {
            Vector3 temp = new Vector3(left, intermed.y, -10f);
            intermed = temp;
        }
        if (intermed.y > up)
        {
            Vector3 temp = new Vector3(intermed.x, up, -10f);
            intermed = temp;
        }
        if (intermed.y < down)
        {
            Vector3 temp = new Vector3(intermed.x, down, -10f);
            intermed = temp;
        }
        transform.position = intermed;
    }
}


