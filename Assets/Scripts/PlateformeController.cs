﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateformeController : MonoBehaviour
{
    public float speed;
    public float waitTime;
    public float startWaitTime;

    public Transform[] destination;
    private int destinationIndex;
    private int chemin;
    public float spot = 3f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, destination[destinationIndex].position, speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, destination[destinationIndex].position) < spot)
        {
            if (waitTime <= 0)
            {
                chemin++;
                if (chemin >= destination.Length)
                {
                    chemin = 0;
                }
                destinationIndex = chemin;
                waitTime = startWaitTime;
            }
        }

            else
            {
                waitTime -= Time.deltaTime;
             }
        }
}
