﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IA_Luciole : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 6f;
    public float radius = 1f;

    public bool IsPlayerNear(Vector3 target)
    {
        Collider2D[] colliders;

        if ((colliders = Physics2D.OverlapCircleAll(target, radius)).Length > 1)
            foreach (var collider in colliders)
            {
                if (collider.gameObject.CompareTag("Player"))
                    return (true);
            }
            return (false);
    }
    public GameObject FindClosestPoint()
    {
        GameObject[] pts;
        GameObject player;

        pts = GameObject.FindGameObjectsWithTag("FireflyCheckpoint");
        player = GameObject.FindGameObjectWithTag("Player");
        GameObject closest = this.gameObject;
        float distance = Mathf.Infinity;
        Vector3 position = player.transform.position;
        foreach (GameObject pt in pts)
        {
            if (IsPlayerNear(pt.transform.position) ||
            pt.GetComponent<FireflyBehavior>().Timer < pt.GetComponent<FireflyBehavior>().Uptime)
                continue;
            Vector3 diff = pt.transform.position - position;
            float currDistance = diff.sqrMagnitude;
            if (currDistance < distance)
            {
                closest = pt;
                distance = currDistance;
            }
        }
        return closest;
    }

    void Start()
    {
        GameObject closest = FindClosestPoint();
        transform.position = Vector2.MoveTowards(transform.position, closest.transform.position, Time.deltaTime * speed);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject closest = FindClosestPoint();
        transform.position = Vector2.MoveTowards(transform.position, closest.transform.position, Time.deltaTime * speed);
    }
}
