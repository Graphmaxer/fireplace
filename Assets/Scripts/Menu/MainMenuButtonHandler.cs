﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtonHandler : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene("LV1");
    }

    public void OpenCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    
    public void Quit()
    {
        Application.Quit();
    }
}