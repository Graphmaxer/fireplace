﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndAnimation : MonoBehaviour
{
    [SerializeField] 
    public float speed = 2f;

    [SerializeField] 
    public Vector3 waypoint;
    private bool launchCountDown = false;
    private float endAnimationDuration = 3;

    void Start()
    {
        transform.position = Vector2.MoveTowards(transform.position, waypoint,
            Time.deltaTime * speed);
    }

    void Update()
    {
        if ((Vector2)transform.position != (Vector2)waypoint) {
        transform.position = Vector2.MoveTowards (transform.position, waypoint,
            speed * Time.deltaTime);
        } else if (!launchCountDown) {
            this.transform.localScale *= 2;
            launchCountDown = true;
        } else {
            CountDown();
        }
    }
    void CountDown()
    {
        if (endAnimationDuration >= 0)
            endAnimationDuration -= Time.deltaTime;
        else
            SceneManager.LoadScene("MainMenu");
    }
}
