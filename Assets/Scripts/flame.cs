﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flame : MonoBehaviour
{
    private float reduceSpeed = 0.02F;
    private float lifeTime = 0.5F;
    private float currentLifeTime;
    private Vector3 originScale;
    private int time;
    public int degats=10;
    private int safe=100;
    public bool gameEnd = false;

    // Start is called before the first frame update
    void Start()
    {
        currentLifeTime = lifeTime;
        originScale = this.transform.localScale;
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        currentLifeTime -= Time.deltaTime;
        if (currentLifeTime <= 0)
        {
            LoseHealthPoint();
        }
    }

    public void respawn()
    {
        this.transform.localScale = originScale;
        Vector3 respawnPoint = this.FindLastPoint().transform.position;
        if (respawnPoint != null)
            this.transform.position = respawnPoint;
    }
    public void LoseHealthPoint()
    {
        currentLifeTime = lifeTime;
        if (this.transform.localScale.x < 0.005)
        {
            respawn();
        }
        else
        {
            if(time >= degats && time <= safe)
            {
                time++;
            }
            if(time > safe)
            {
                time = 0;
            }
            if (time < degats)
            {
                // Debug.Log(this.transform.localScale);
                this.transform.localScale -= Vector3.one * reduceSpeed;
                time++;
            }
        }
    }

    public GameObject FindLastPoint()
    {
        GameObject[] pts;

        pts = GameObject.FindGameObjectsWithTag("FireflyCheckpoint");
        float earliestTimer = Mathf.Infinity;
        GameObject earliestPoint = null;
        foreach (GameObject pt in pts)
        {
            float pointTimer = pt.GetComponent<FireflyBehavior>().Timer;
            if (pointTimer < earliestTimer)
                earliestPoint = pt;
            earliestTimer = pointTimer;
        }
        return earliestPoint;
    }

    public void Heal()
    {
        currentLifeTime = lifeTime;
        if (this.transform.localScale.x < 1)
        {
            Debug.Log(this.transform.localScale);
            this.transform.localScale += Vector3.one * 0.3F;
            Debug.Log(this.transform.localScale);
        }
    }

    public bool IsRemainingEmber()
    {
        foreach (GameObject ember in GameObject.FindGameObjectsWithTag("Ember")) {
            return true;
        }
        return false;
    }

    void ToogleBoxCollider()
    {
        Collider2D collider;
        collider = this.GetComponent<Collider2D>();
        collider.enabled = !collider.enabled;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Fireplace") && !IsRemainingEmber() && !gameEnd) {
            if (GetComponent("PlayerMovement")) {
                Destroy(GetComponent("PlayerMovement"));
            }
            foreach (Transform child in transform)
            {
                if (child.CompareTag("Eyes"))
                    Destroy(child.gameObject);
            }
            EndAnimation autoMovement = gameObject.AddComponent<EndAnimation>() as EndAnimation;
            Vector3 endingPoint = new Vector3();
            endingPoint = col.gameObject.transform.position + new Vector3(0.33F, 1.87F, 0.0F);
            autoMovement.waypoint = endingPoint;
            autoMovement.speed = 2f;
            this.transform.localScale = originScale;
            lifeTime = Mathf.Infinity;
            ToogleBoxCollider();
            gameEnd = true;
        }
    }
}