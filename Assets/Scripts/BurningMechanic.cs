﻿using UnityEngine;

public class BurningMechanic : MonoBehaviour
{
    public float radiusToCheck = 1.5f;
    public float timeToBurnElements = 1.0f;
    private float _timeWhileStaying = 0.0f;

    void Start()
    {
    }

    void Update()
    {
        var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        if (move != Vector3.zero)
        {
            _timeWhileStaying = 0;   
        }
        else
        {
            _timeWhileStaying += Time.deltaTime;
        }
        if (_timeWhileStaying > timeToBurnElements)
        {   
            Collider2D[] colliders;
            if ((colliders = Physics2D.OverlapCircleAll(transform.position, radiusToCheck)).Length > 1)
            {
                foreach (var collider in colliders)
                {
                    var nearObject = collider.gameObject;
                    if (nearObject == gameObject || !nearObject.CompareTag("Burnable"))
                        continue;
                    if (nearObject.transform.localScale.x < 0.6)
                    {
                        Destroy(nearObject);
                    }
                    else
                    {
                        nearObject.transform.localScale -= Vector3.one * 0.2f;
                    }
                }
            }
            _timeWhileStaying = 0.0f;
        }
    }
}
