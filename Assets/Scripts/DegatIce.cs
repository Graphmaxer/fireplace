﻿using UnityEngine;

public class DegatIce : MonoBehaviour
{
    private float radiusToCheck = 1.2f;
    private GameObject Player;


    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        if (Physics2D.IsTouching(gameObject.GetComponent<Collider2D>() , Player.GetComponent<Collider2D>()))
        {
               (Player.GetComponent("flame") as flame).LoseHealthPoint();   
        }
    }
}


