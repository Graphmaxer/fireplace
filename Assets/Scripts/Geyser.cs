﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Geyser : MonoBehaviour
{
    float Timer;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        Timer += Time.deltaTime;
        if (Timer > 3)
        {
            if (!ps.isPlaying)
            {
                ps.Play();
            }
            else
            {
                ps.Stop();
            }
            Timer = 0;
        }
    }
}
