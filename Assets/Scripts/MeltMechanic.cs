﻿using UnityEngine;

public class MeltMechanic : MonoBehaviour
{
    public float radiusToCheck = 2f;
    public float meltSpeed = 0.02f;
    void Start()
    {
    }

    void Update()
    {
        Collider2D[] colliders;
        if ((colliders = Physics2D.OverlapCircleAll(transform.position, radiusToCheck)).Length > 1)
        {
            foreach (var collider in colliders)
            {
                var nearObject = collider.gameObject;
                if (nearObject == gameObject || !nearObject.CompareTag("Meltable"))
                    continue;
                if (nearObject.transform.localScale.x < 0.001)
                {
                    Destroy(nearObject);
                }
                else
                {
                    nearObject.transform.localScale -= Vector3.one * meltSpeed * Time.deltaTime;
                }
            }
        }
    }
}