﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Braise : MonoBehaviour
{
    public float distance = 0.5f;
    public float grab = 3f;
    public float speed = 2f;

    private Transform player;

    private GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if(Vector3.Distance(transform.position, player.position) <= grab)
        {
            Vector3 dir = player.position - transform.position;
            transform.Translate(dir.normalized * speed * Time.deltaTime);
        }
            if (Vector3.Distance(transform.position, player.position) <= distance)
        {
            (Player.GetComponent("flame") as flame).Heal();
            Destroy(gameObject);
        }
    }
}
