﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionWater : MonoBehaviour
{
    public float range = 10f;
    private Transform player;
    private GameObject Water;
    // Start is called before the first frame update
    void Start()
    {
        Water = GameObject.FindGameObjectWithTag("Water");
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (Vector3.Distance(transform.position, player.position) <= range)
        {
            (Water.GetComponent("Water") as Water).validateUp();
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
