﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    private int Up;
    private float ActualUp;
    public float time = 5f;
    public float speed = 3f;
    public float UpEnd = 5f;
    // Start is called before the first frame update
    void Start()
    {
        Up = 0;
        ActualUp = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(Up == 1)
        {
            if (ActualUp <= UpEnd)
            {
                transform.Translate(Vector3.up * speed * Time.deltaTime);
                ActualUp = ActualUp + speed*Time.deltaTime;
            }
        }

    }

    public void validateUp()
    {
        Up = 1;
    }
}
