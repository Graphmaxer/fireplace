﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireflyBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public float Timer = 5f;
    public float radius = 0.1f;
    public float Uptime = 5f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Collider2D[] colliders;

        Timer += Time.deltaTime;
        if ((colliders = Physics2D.OverlapCircleAll(transform.position, radius)).Length > 1)
            foreach (var collider in colliders)
            {
                if (collider.gameObject.CompareTag("Player"))
                    Timer = 0;
            }
    }
}
